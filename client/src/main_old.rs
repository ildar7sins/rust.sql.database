// use mini_redis::{client, Result};
//
// #[tokio::main]
// async fn main() -> Result<()> {
//     // Open a connection to the mini-redis address.
//     let mut client = client::connect("127.0.0.1:8080").await?;
//
//     // Set the key "hello" with value "world"
//     // client.set("hello", "world".into()).await?;
//
//     // Get key "hello"
//     let result = client.get("hello").await?;
//     //
//     println!("got value from the server; result={:?}", result);
//
//     Ok(())
// }






// use std::net::{TcpStream};
// use std::io::{Read, Write};
// use std::str::from_utf8;
//
// fn main() {
//     match TcpStream::connect("127.0.0.1:8080") {
//         Ok(mut stream) => {
//             println!("Successfully connected to server in port 8080");
//
//             let msg = b"Hello!";
//
//             stream.write(msg).unwrap();
//             println!("Sent Hello, awaiting reply...");
//
//             let mut data = [0 as u8; 6]; // using 6 byte buffer
//             match stream.read_exact(&mut data) {
//                 Ok(_) => {
//                     if &data == msg {
//                         println!("Reply is ok!");
//                     } else {
//                         let text = from_utf8(&data).unwrap();
//                         println!("Unexpected reply: {}", text);
//                     }
//                 },
//                 Err(e) => {
//                     println!("Failed to receive data: {}", e);
//                 }
//             }
//         },
//         Err(e) => {
//             println!("Failed to connect: {}", e);
//         }
//     }
//     println!("Terminated.");
// }





//! Hello world server.
//!
//! A simple client that opens a TCP stream, writes "hello world\n", and closes
//! the connection.
//!
//! You can test this out by running:
//!
//!     ncat -l 6142
//!
//! And then in another terminal run:
//!
//!     cargo run --example hello_world

#![warn(rust_2018_idioms)]

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

use std::error::Error;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
    // Open a TCP stream to the socket address.
    //
    // Note that this is the Tokio TcpStream, which is fully async.
    let mut stream = TcpStream::connect("127.0.0.1:8080").await?;
    println!("created stream");

    let result = stream.write(b"hello world").await;
    println!("wrote to stream; success={:?}", result.is_ok());

    Ok(())
}


pub fn send_request(request)
{
}
