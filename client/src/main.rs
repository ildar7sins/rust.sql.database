use rustyline::Editor;
use crate::models::request::Query;

mod models;

pub type Error = Box<dyn std::error::Error + Send + Sync>;
pub type Result<T> = std::result::Result<T, Error>;

#[tokio::main]
pub async fn main() -> Result<()> {
    let current_request: String = String::from("");
    let _request = new_request(current_request, "sql: ").await;

    // println!("{:?}", request);
    Ok(())
}
use futures::future::{BoxFuture};
use async_recursion::async_recursion;

#[async_recursion]
async fn new_request(mut current_request: String, line_txt: &str) -> BoxFuture<'static, ()> {
    let mut rl = Editor::<()>::new();
    let request = rl.readline(line_txt).unwrap_or(String::from(""));

    if request == String::from("exit") && current_request == String::from("") {
        std::process::exit(0);
    }

    let res: &str = match request.chars().last().unwrap_or(' ') {
        ';' => {
            let str = request.clone();
            current_request.push_str(str.as_str());
            let _res = Query::new(current_request.clone()).execute().await;

            current_request = String::from("");
            "sql: "
        }
        _ => {
            let str = request.clone();
            current_request.push_str(" ");
            current_request.push_str(str.as_str());
            "   -> "
        }
    };

    new_request(current_request, res).await
}
