

#[derive(Debug)]
pub enum ResponseStatus {
    FAIL,
    ERROR,
    SUCCESS,
    UNKNOWN,
}

impl ResponseStatus {
    pub(crate) fn create(status: &str) -> ResponseStatus {
        match status.to_uppercase().as_str() {
            "FAIL" => ResponseStatus::FAIL,
            "ERROR" => ResponseStatus::ERROR,
            "OK" => ResponseStatus::SUCCESS,
            _ => ResponseStatus::UNKNOWN
        }
    }

    pub fn _get_value(&self) -> &str {
        match *self {
            ResponseStatus::FAIL => "FAIL",
            ResponseStatus::ERROR => "ERROR",
            ResponseStatus::SUCCESS => "OK",
            ResponseStatus::UNKNOWN => "UNKNOWN",
        }
    }

    pub fn is_success(&self) -> bool {
        matches!(self, ResponseStatus::SUCCESS)
    }

    pub fn is_error(&self) -> bool {
        matches!(self, ResponseStatus::ERROR)
    }
}