use std::io;
use serde_json;
use serde_json::Result;
use crate::models::response::Response;
use crate::models::response_data_type::ResponseDataType;
use crate::models::response_status::ResponseStatus;

pub struct ResponsePrinter {
    pub response: Response
}

impl ResponsePrinter {
    pub fn build(response: Response) -> ResponsePrinter
    {
        ResponsePrinter { response }
    }

    pub fn print(&self)
    {
        let status: ResponseStatus = ResponseStatus::create(&*self.response.status.clone().as_str());
        let data_type: ResponseDataType = ResponseDataType::create(&*self.response.data_type.clone().as_str());

        if status.is_success() {
            match data_type {
                ResponseDataType::TABLE => self.print_table(),
                ResponseDataType::STRING => println!("{}", self.response.data),
                ResponseDataType::UNKNOWN => println!("Unknown data type, try again...")
            };
        } else if status.is_error() {
            println!("There's an error, but i don't know what to do with it...");
        } else {
            println!("Unknown response status, try again...");
        }
    }

    fn print_table(&self)
    {
        let data_string: String = self.response.data.clone();
        let data: Result<Vec<Vec<&str>>> = serde_json::from_str(data_string.as_str());
        let res = data.unwrap_or(Vec::new());

        text_tables::render(&mut io::stdout(),  res).unwrap();
    }

}