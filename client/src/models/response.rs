use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Response {
    pub status: String,
    pub data: String,
    pub data_type: String,
}
