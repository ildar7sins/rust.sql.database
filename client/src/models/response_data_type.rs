

#[derive(Debug)]
pub enum ResponseDataType {
    TABLE,
    STRING,
    UNKNOWN
}

impl ResponseDataType {
    pub fn create(status: &str) -> ResponseDataType {
        match status.to_uppercase().as_str() {
            "TABLE" => ResponseDataType::TABLE,
            "STRING" => ResponseDataType::STRING,
            _ => ResponseDataType::UNKNOWN
        }
    }

    pub fn _get_value(&self) -> &str {
        match *self {
            ResponseDataType::TABLE => "TABLE",
            ResponseDataType::STRING => "STRING",
            ResponseDataType::UNKNOWN => "UNKNOWN",
        }
    }
}