extern crate text_tables;

use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;
use crate::models::response::{Response};
use crate::models::response_printer::ResponsePrinter;

pub struct Query {
    sql: String
}

impl Query {
    pub fn new(sql: String) -> Query {
        Query {
            sql
        }
    }

    pub async fn execute(&self) -> std::result::Result<(), Box<(dyn std::error::Error + 'static)>> {
        let mut stream = TcpStream::connect("127.0.0.1:8080").await?;
        let _result = stream.write(self.sql.as_ref()).await;

        let mut buf = [0; 1024];
        let n: usize = stream.read(&mut buf).await.unwrap_or(0);
        let data = String::from_utf8_lossy(&buf[0..n]).to_string();
        let response: Response = serde_json::from_str(data.as_ref())?;
        let response_printer = ResponsePrinter::build(response);

        response_printer.print();

        Ok(())
    }
}
