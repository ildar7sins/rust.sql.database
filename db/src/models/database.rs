use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct DbResult {
    pub status: String,
    pub data: String,
    pub data_type: String,
}

// const STATUS_FAIL: String = String::from("FAIL");
// const STATUS_SUCCESS: String = String::from("OK");
