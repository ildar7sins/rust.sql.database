use crate::models::request::request_type::RequestType;

#[derive(Debug)]
pub struct Request {
    req_type: RequestType,
    columns: Option<String>,
    from: Option<String>,
}

impl Request {
    pub fn create(request_type: RequestType) -> Request {
        Request {
            req_type: request_type,
            columns: None,
            from: None,
        }
    }
}


// "with": null,
// "options": null,
// "distinct": null,
// "from": [
// {
// "db": null,
// "table": "t",
// "as": null
// }
// ],
// "where": null,
// "groupby": null,
// "having": null,
// "orderby": null,
// "limit": null