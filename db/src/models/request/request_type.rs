use crate::results::request_result::RequestTypeResult;

#[derive(Debug)]
pub enum RequestType {
    SELECT,
    INSERT,
    UPDATE,
    DELETE,

    #[allow(non_camel_case_types)]
    CREATE_INDEX,
    #[allow(non_camel_case_types)]
    DELETE_INDEX,
}

pub fn get_request_type(command: String) -> RequestTypeResult
{
    let check = command.to_lowercase();

    match check.as_str() {
        "select" => Ok(RequestType::SELECT),
        "insert" => Ok(RequestType::INSERT),
        "update" => Ok(RequestType::UPDATE),
        "delete" => Ok(RequestType::DELETE),

        "create_index" => Ok(RequestType::CREATE_INDEX),
        "delete_index" => Ok(RequestType::DELETE_INDEX),
        _ => Err("Unknown command")
    }
}
