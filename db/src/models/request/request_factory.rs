use crate::get_request_type;
use crate::models::request::request::Request;
use crate::results::request_result::RequestResult;

pub struct RequestFactory {
    pub(crate) request: String
}

impl RequestFactory {
    pub fn build(&self) -> RequestResult
    {
        let request = self.request.clone();
        let request_parts = request.split_whitespace();

        // Parse request type...
        let request_type = get_request_type(String::from(request_parts.next().unwrap_or("")));
        if let Err(e) = request_type {
            return Err(e);
        }

        let request: Request = Request::create(request_type?);
        println!("{:?}", request);


        Ok(request)
    }
}