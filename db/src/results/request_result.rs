use crate::models::request::request::Request;
use crate::models::request::request_type::RequestType;

pub type RequestResult = Result<Request, &'static str>;
pub type RequestTypeResult = Result<RequestType, &'static str>;