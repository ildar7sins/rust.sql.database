mod models;
mod results;

use tokio::net::TcpListener;
use crate::models::database::DbResult;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use crate::models::request::request_type::get_request_type;
use crate::models::request::request_factory::RequestFactory;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let listener = TcpListener::bind("127.0.0.1:8080").await?;

    loop {
        let (mut stream, _) = listener.accept().await?;

        tokio::spawn(async move {
            let mut buf = [0; 1024];

            loop {
                let n = match stream.read(&mut buf).await {
                    // stream closed
                    Ok(n) if n == 0 => return,
                    Ok(n) => n,
                    Err(e) => {
                        eprintln!("failed to read from stream; err = {:?}", e);
                        return;
                    }
                };
                let data: String = String::from_utf8_lossy(&buf[0..n]).to_string();

                // Где то здесь мы обращаемся к БД и получаем какой то Result...
                let request: RequestFactory = RequestFactory { request: data.clone() };
                let _unused_data = request.build();
                // println!("{:?}", unused_data);






                let resp = [["id", "name", "age"], ["1", "Ildar Gaskarov", "30"]];

                let resp2 = serde_json::to_string(&resp).unwrap_or(String::from(""));

                let res = DbResult {
                    status: String::from("OK"),
                    data: resp2,
                    data_type: String::from("TABLE")
                };

                let resp3 = serde_json::to_string(&res).unwrap_or(String::from(""));
                let _res = stream.write(resp3.as_ref()).await;


                return
            }
        });
    }
}
